:toc:
:toc-title: Contenido
:icons: font
:source-highlighter: highlight.js
:experimental:
:leveloffset: 0.


== MEJORA DE INTERFAZ

=== Cambio de tipo de letra:
** Elección del tipo de letra: Se selecciono la fuente de tipo `consola` para nuestra aplicación, ya que proporciona una estética adecuada y profesional para nuestra aplicación.

** Color de Letra: El color seleccionado para el texto es un tono celeste, que no solo combina con el logotipo de nuestra aplicación, sino que también mejora la visibilidad y el contraste, facilitando la lectura y escritura.


NOTE: *Aclaracion:* La uniformidad en el tamaño de los caracteres de la fuente `consola`, independientemente de que sean mayúsculas o minúsculas, nos permite calcular con precisión la longitud de los textos. Esto es especialmente útil para optimizar el espacio disponible dentro de los botones de inicio de sección, donde se mostrarán el nombre de usuario y su correo electrónico correspondiente. 


=== Cambios de botones:
** Estética: Se implementó un nuevo diseño para los botones, optando por formas más cuadradas en lugar de redondeadas, Esto amplía el ancho de los botones, permitiendo así un espacio adecuado para ingresar tanto el nombre de usuario como el correo electrónico.


** Tipografía: Continuamos utilizando la fuente Consola por su claridad y hemos incrementado el tamaño de la letra del nombre de usuario para mejorar la legibilidad.

** Organización de la Información: El correo electrónico del usuario ahora se muestra debajo del nombre, pero con un tamaño menor que el nombre de usuario, lo que facilita la distinción entre usuarios con nombres idénticos, ya que sus correos serán únicos.

=== Sócalo de sistema:
** Espacio de Mensajes: Se ha diseñado un área específica para mensajes del sistema que orientará al usuario durante el proceso de registro, informando sobre cualquier incidencia. Este sócalo de sistema se ha incluido como una característica deseable en la tarjeta de diseño 2.5.

=== Optimización del Buscador de Usuarios:
** Búsqueda por Nombre de Cuenta: Hemos integrado una función de búsqueda que permite localizar una cuenta de usuario específica mediante su nombre. Esta herramienta es especialmente útil en situaciones donde hay numerosas cuentas, simplificando así la experiencia del usuario al identificar y acceder a la cuenta deseada con mayor rapidez y eficiencia

=== Logo de la aplicación:

** El logo de aplicación se ha incorporado tanto en la pantalla de registro como en la de inicio de sección, aportando una imagen más profesional al diseño general.
