package com.example.cypher_vault.view.navigation


import android.util.Log
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.cypher_vault.controller.authentication.AuthenticationController
import com.example.cypher_vault.view.login.NavigationLogin
import com.example.cypher_vault.view.registration.CameraLoginPreviewScreen
import com.example.cypher_vault.view.registration.CameraRegisterPreviewScreen
import com.example.cypher_vault.view.registration.ConfirmationScreen
import com.example.cypher_vault.view.gallery.GalleryScreen
import com.example.cypher_vault.view.registration.InitialScreen

@Composable
fun NavigationHost() {
    val navController = rememberNavController()
    val authenticationController = AuthenticationController(navController)
    NavHost(navController, startDestination = "register") {
        composable("register") {
            Log.d("faceDetection", "Inicio InitialScreen")
            InitialScreen(authenticationController)
            Log.d("faceDetection", "Salida InitialScreen")
        }
        composable("cameraR/{userId}") { backStackEntry ->
            val userId = backStackEntry.arguments?.getString("userId")
            if (userId != null) {
                Log.d("faceDetection", "Inicio CameraPreviewScreen")
                CameraRegisterPreviewScreen(authenticationController, userId)
                Log.d("faceDetection", "Salida CameraPreviewScreen")
            } else {
                // Manejar el caso en que el userId no se pudo recuperar
            }
        }
        composable("cameraL/{userId}") { backStackEntry ->
            val userId = backStackEntry.arguments?.getString("userId")
            if (userId != null) {
                Log.d("faceDetection", "Inicio CameraPreviewScreen")
                CameraLoginPreviewScreen(authenticationController, userId)
                Log.d("faceDetection", "Salida CameraPreviewScreen")
            } else {
                // Manejar el caso en que el userId no se pudo recuperar
            }
        }
        composable("confirmation/{userId}") {
            Log.d("faceDetection", "Inicio ConfirmationScreen")
            ConfirmationScreen(authenticationController)
            Log.d("faceDetection", "Salida ConfirmationScreen")
        }
        composable("login") {
            Log.d("faceDetection", "Inicio login")
            // Aquí puedes agregar la vista de inicio de sesión
        }
        composable("galery") {
            Log.d("faceDetection", "Inicio Galery")
            GalleryScreen(authenticationController)
            Log.d("faceDetection", "Salida Galery")
        }
        composable("list") {
            Log.d("faceDetection", "Inicio NavigationLogin")
            NavigationLogin(authenticationController)
            Log.d("faceDetection", "Salida NavigationLogin")
        }
    }
}