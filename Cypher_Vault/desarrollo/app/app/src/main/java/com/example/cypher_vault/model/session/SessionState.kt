package com.example.cypher_vault.model.session

data class SessionState(
    val isLoggedIn: Boolean = true
)