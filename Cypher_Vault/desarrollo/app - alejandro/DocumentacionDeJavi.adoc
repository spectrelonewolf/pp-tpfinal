:toc:
:toc-title: Contenido
:icons: font
:source-highlighter: highlight.js
:experimental:
:leveloffset: 0.



= INTERFAZ

== INTRODUCCIÓN
** El código se divide en tres paquetes que se encuentran en `app > src > main > java > com.example.cypher_vault`. Acá hay dos paquetes: uno llamado 'controller' y otro llamado 'view'.

== Paquete Controller > Authentication

=== AuthenticationController

```kotlin
AuthenticationController
```

`AuthenticationController` es una clase que toma como parámetro un `NavController` y devuelve el `NavController` con la dirección a la que debe navegar. 
Cada dirección tiene su propia función: `fun navigateToCamera()`, `fun navigateToConfirmation()`, `fun navigateToLogin()` (esta última falta implementar).


```kotlin
fun registerUser(
        email: String,
        name: String,
        showDialog: MutableState<Boolean>,
        errorMessage: MutableState<String>
    )
```

La función `registerUser` valida los campos por el momento. Más adelante deberá enviarlos al modelo para guardarlos en la base de datos. 
Recibe como parámetros `email`, `name`, `showDialog` y `errorMessage`. Estos parámetros son para que salga la alerta y mostrarla con sus respectivos mensajes.
Si todos los campos están bien, llama a `navigateToCamera` y los manda a la cámara.

Las funciones `validateMail()`, `validateName()` y `validateFields` verifican la validez de los campos de entrada.

- `validateMail(email: String)`: Se fija que se cumpla android.util.Patterns.EMAIL_ADDRESS.matcher(email).
- `validateName(name: String)`: Se fija que no tenga menos de 3 carácteres el nombre.
- `validateFields(name: String, email: String)`: Se fija que no esten vacios.


== Paquete View > Registration



=== NavigationHost.kt

```kotlin
fun NavigationHost()
```

`NavigationHost()` es una función que se utiliza para manejar la navegación en la aplicación, cada vez que se presiona un botón cambia las pantallas. 

Empieza en register por predeterminado y luego va cambiando, toma como parámetro las direcciones que le pasa el AuthenticationController, .

* **Definición de pantallas**: Dentro de esta función `NavHost`, se definen varias pantallas que representan diferentes partes:
** *register*: Esta es la pantalla inicial donde los usuarios pueden registrarse. Muestra `InitialScreen`.
** *camera* Esta es la pantalla donde los usuarios pueden usar la cámara durante el proceso de registro. Muestra `RegistrationCameraScreen`.
** *confirmation*: Esta es la pantalla donde los usuarios pueden confirmar su registro. Muestra `ConfirmationScreen`.
** *login*: Esta es la pantalla donde los usuarios pueden iniciar sesión. Falta implementar.


=== InitialScreen.kt


```kotlin
fun RegistrationCameraScreen(authenticationController: AuthenticationController)
```

Recibe como parametro authenticationController para luego poder navegar por la aplicacion

`InitialScreen` es la pantalla inicial donde los usuarios se van a registrar. Se encuentran los campos de entrada para el correo electrónico y el nombre. Al hacer clic en el botón "Registrarse", se llama al método `registerUser` del 
`AuthenticationController`.


=== RegistrationCameraScreen.kt

```kotlin
fun RegistrationCameraScreen(authenticationController: AuthenticationController)
```

Recibe como parametro authenticationController para luego poder navegar por la aplicacion

Esta función Muestra la vista previa de la cámara
`ProcessCameraProvider:` Esta es una clase que se utiliza para interactuar con las cámaras disponibles en el dispositivo. En este caso, se obtiene una instancia de ProcessCameraProvider y 
se recuerda para su uso posterior.

`CameraSelector:` Esta es una clase que se utiliza para seleccionar una cámara en el dispositivo. En este caso, se está seleccionando la cámara frontal.




```kotlin
fun CloseCameraButton(isCameraOpen: MutableState<Boolean>,
                      cameraProvider: ProcessCameraProvider,
                        authenticationController: AuthenticationController)
```

Botón que se muestra para cerrar la cámara e ir a la parte de ConfirmationScreen



```kotlin
fun CameraPreview(preview: Preview)
```

Muestra la vista previa de la cámara en la interfaz de usuario. Utiliza la clase AndroidView para mostrar la vista previa de la cámara en la interfaz de usuario de Compose.



=== ConfirmationScreen.kt



```kotlin
fun ConfirmationScreen(authenticationController: AuthenticationController)
```


Recibe como parametro authenticationController para luego poder navegar por la aplicacion

`ConfirmationScreen` Es una pantalla que muestra un mensaje de que se pudo registrar y un botón para iniciar sesión


== Paquete View > Login
=== LoginList.kt
```kotlin
fun NavigationLogin(authenticationController: AuthenticationController) 
```
`NavigationLogin()`: Esta función se encarga de mostrar un lista de los usuarios que ya están registrados en la aplicación. Permite a los usuarios navegar a través de sus cuentas de forma eficiente.  

```kotlin
fun loginCamera(authenticationController: AuthenticationController, user: String) 
```
La función `loginCamera` se activa después de que el usuario ha seleccionado su cuenta. Su propósito es encender la cámara frontal para realizar una verificación biométrica,asegurándose de que la cuenta seleccionada pertenezca realmente al usuario en cuestión. Esta validación permite mantener la seguridad y la integridad de la cuenta.

```kotlin
fun CloseCameraButton(cameraProvider: ProcessCameraProvider, authenticationController: AuthenticationController)
```
La función `CloseCameraButton` permite al usuario cerrar la cámara frontal si se ha seleccionado una cuenta incorrecta. Ofrece una interfaz para regresar de manera rápida al inicio de sesión, específicamente a la pantalla de `NavigationLogin`, facilitando el desplazamiento dentro de la aplicación.


## Links

https://developer.android.com/develop/ui/compose
https://developer.android.com/reference/androidx/navigation/NavController
https://developer.android.com/reference/kotlin/androidx/compose/runtime/Composable
https://developer.android.com/reference/android/util/Patterns
